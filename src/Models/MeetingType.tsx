import { createContext, useState } from "react";
import { Service, ServicesContextType, ServicesContext } from "./Service";

export const MeetingServicesContext = createContext<ServicesContextType | null>(null);

const MeetingServiceProvider = ({ children }: { children: JSX.Element }) => {
    const [services, setServices] = useState<Service[]>([
        {
            name: 'Haircut',
            duration: '30m-60m',
            price: 'Starting from ₹ 999.00',
            notes: 'Served at customer location ● Group service',
            requiresPrePayment: true,
        },
        {
            name: 'Bridal Makeup',
            duration: '30m',
            price: '₹ 3,0001.00',
            notes: 'Online Service',
            requiresPrePayment: true
        },
        {
            name: 'Ready for Garba',
            duration: '30m',
            price: '₹ 499.00',
            notes: 'At business location',
            requiresPrePayment: true
        },
    ]);

    return <MeetingServicesContext.Provider value={{ services, setServices }}>
        {children}
    </MeetingServicesContext.Provider>
}

export default MeetingServiceProvider;
