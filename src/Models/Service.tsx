import { createContext, useState } from "react";

export interface Service {
    name: string,
    duration: string,
    price: string,
    notes: string,
    requiresPrePayment: boolean,
}

export interface ServicesContextType {
    services: Service[],
    setServices: React.Dispatch<React.SetStateAction<Service[]>>
}

export const ServicesContext = createContext<ServicesContextType | null>(null);

const ServiceProvider = ({ children }: { children: JSX.Element }) => {
    const [services, setServices] = useState<Service[]>([
        { name: 'Haircut', duration: '30m', price: '₹ 799', notes: '', requiresPrePayment: false },
        { name: 'Haircut Color', duration: '45m', price: '₹ 1299', notes: '', requiresPrePayment: false },
        { name: 'Haircut Spa', duration: '120m', price: '₹ 1599', notes: 'Group Service', requiresPrePayment: false },
    ]);

    return <ServicesContext.Provider value={{ services, setServices }}>
        {children}
    </ServicesContext.Provider>
}

export default ServiceProvider;
