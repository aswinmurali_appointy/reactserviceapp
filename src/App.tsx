import DeleteService, { DeleteServiceProvider } from "./Components/DeleteService";
import ListMeetingType from "./Components/ListMeetings";
import ListServices from "./Components/ListServices";
import ReviewBox from "./Components/ReviewBox";
import MeetingServiceProvider from "./Models/MeetingType";
import ServiceProvider from "./Models/Service";

export const Providers = ({ children }: { children: JSX.Element }) => (
  <ServiceProvider>
    <MeetingServiceProvider>
      <DeleteServiceProvider>
        {children}
      </DeleteServiceProvider>
    </MeetingServiceProvider>
  </ServiceProvider>
)

const App = () => <Providers>
  <>
    <DeleteService />
    <ListServices />
    <ListMeetingType />
    <ReviewBox />
  </>
</Providers>

export default App;
