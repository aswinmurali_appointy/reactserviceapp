import { Accordion, AccordionDetails, AccordionSummary, Avatar, Button, CardHeader, Container, Grid, IconButton, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, makeStyles, Typography } from '@material-ui/core';
import { CreditCard, Delete, ExpandMore } from '@material-ui/icons';
import React from 'react';
import { useContext } from 'react';
import MeetingServiceProvider, { MeetingServicesContext } from '../Models/MeetingType';
import { ServicesContext, ServicesContextType } from '../Models/Service';
import { DeleteServiceContext, DeleteServiceContextType } from './DeleteService';

const useStyle = makeStyles({
    root: {
        paddingBottom: '10px',
    },
    button: {
        marginLeft: '20px',
        color: '#3275e0',
    },
    title: {
        margin: '30px',
    },
    list: {
        width: '100%',
    },
    prepayment: {
        color: 'grey',
    }
});

const ListMeetingType = () => {
    const style = useStyle();

    const { services } = useContext(MeetingServicesContext) as ServicesContextType;
    const { setDeleteService } = useContext(DeleteServiceContext) as DeleteServiceContextType;

    const [expanded, setExpanded] = React.useState<string | false>('panel1');

    const handleChange =
        (panel: string) => {
            return (event: React.SyntheticEvent, isExpanded: boolean) => {
                setExpanded(isExpanded ? panel : false);
            };
        };

    const randomColor = () => {
        let hex = Math.floor(Math.random() * 0xFFFFFF);

        return "#" + hex.toString(16);
    }

    return <Container>
        <Typography className={style.title}>SELECT MEETING TYPE</Typography>
        <Accordion elevation={0} expanded={expanded === 'panel1'} onChange={() => handleChange('panel1')}>
            <AccordionSummary
                expandIcon={<ExpandMore />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
            >
                <CardHeader
                    avatar={<Avatar>B</Avatar>}
                    title='BRIDAL SERVICES'
                />
            </AccordionSummary>
            <AccordionDetails>
                <List className={style.list}>
                    {services.map(
                        (service) => <ListItem divider className={style.root}>
                            <Grid container>
                                <Grid item>
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="delete" onClick={() => setDeleteService(true)}>
                                            <Delete />
                                        </IconButton>
                                        <Button variant='outlined' className={style.button}>SELECT </Button>
                                    </ListItemSecondaryAction>
                                    <ListItemAvatar>
                                        <Avatar
                                            style={{ backgroundColor: randomColor() }}
                                            variant="rounded"
                                        >
                                            {service.name[0]}
                                        </Avatar>
                                    </ListItemAvatar>

                                </Grid>
                                <Grid item>
                                    <ListItemText
                                        primary={service.name}
                                        secondary={
                                            service.notes === '' ?
                                                `${service.duration}m ● ${service.price}`
                                                :
                                                `${service.duration}m ● ${service.price} ● ${service.notes}`
                                        }
                                    />
                                    {
                                        service.requiresPrePayment ?
                                            <CardHeader className={style.prepayment} avatar={<CreditCard />} title={'Requires pre-payment'}></CardHeader>
                                            : null
                                    }
                                </Grid>
                            </Grid>
                        </ListItem>
                    )}
                </List >
            </AccordionDetails>
        </Accordion>

        <Accordion elevation={0} expanded={expanded === 'panel1'} onChange={() => handleChange('panel1')}>
            <AccordionSummary
                expandIcon={<ExpandMore />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
            >
                <CardHeader
                    avatar={<></>}
                    title='DEFAULT'
                />
            </AccordionSummary>
        </Accordion>
    </Container>
}

export default ListMeetingType;
