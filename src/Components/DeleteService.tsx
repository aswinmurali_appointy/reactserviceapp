import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, makeStyles } from '@material-ui/core';
import { createContext, useContext, useState } from 'react';

const useDeleteDialogStyle = makeStyles({
    root: {
        width: '500px',
    },
    button: {
        backgroundColor: '#3275e0',
        color: 'white',
    }
});

export type DeleteServiceContextType = {
    openDeleteService: boolean,
    setDeleteService: React.Dispatch<React.SetStateAction<boolean>>
}

export const DeleteServiceContext = createContext<DeleteServiceContextType | null>(null);

export const DeleteServiceProvider = ({ children }: { children: JSX.Element }) => {
    const [openDeleteService, setDeleteService] = useState(false);

    return <DeleteServiceContext.Provider value={{
        openDeleteService, setDeleteService,
    }}>{children}
    </DeleteServiceContext.Provider>
}

const DeleteService = () => {
    const style = useDeleteDialogStyle();

    const { openDeleteService, setDeleteService } = useContext(DeleteServiceContext) as DeleteServiceContextType;

    const handleClose = () => setDeleteService(false);

    return <Dialog open={openDeleteService} >
        <DialogTitle>Delete Service</DialogTitle>
        <DialogContent className={style.root}>
            <DialogContentText id="alert-dialog-description">
                Are you sure you want to continue?
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose}>CANCEL</Button>
            <Button onClick={handleClose} autoFocus variant='contained' className={style.button}>CONFIRM</Button>
        </DialogActions>
    </Dialog>
}

export default DeleteService;
