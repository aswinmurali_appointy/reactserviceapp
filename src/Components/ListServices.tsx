import { Avatar, Button, Container, Grid, IconButton, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, ListSubheader, makeStyles, Typography } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { useContext } from 'react';
import { ServicesContext, ServicesContextType } from '../Models/Service';
import { DeleteServiceContext, DeleteServiceContextType } from './DeleteService';

const useStyle = makeStyles({
    root: {
        paddingBottom: '30px',
    },
    button: {
        marginLeft: '20px',
        color: '#3275e0',
    },
    title: {
        margin: '30px',
    }
});

const ListServices = () => {
    const style = useStyle();

    const { services } = useContext(ServicesContext) as ServicesContextType;
    const { setDeleteService } = useContext(DeleteServiceContext) as DeleteServiceContextType;

    const randomColor = () => {
        let hex = Math.floor(Math.random() * 0xFFFFFF);

        return "#" + hex.toString(16);
    }

    return <Container>
        <Typography className={style.title}>SELECT SERVICE</Typography>
        <List >
            {services.map(
                (service) => <ListItem divider className={style.root}>
                    <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="delete" onClick={() => setDeleteService(true)}>
                            <Delete />
                        </IconButton>
                        <Button variant='outlined' className={style.button}>SELECT </Button>
                    </ListItemSecondaryAction>
                    <ListItemAvatar>
                        <Avatar
                            style={{ backgroundColor: randomColor() }}
                            variant="rounded"
                        >
                            {service.name[0]}
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={service.name}
                        secondary={
                            service.notes === '' ?
                                `${service.duration}m ● ${service.price}`
                                :
                                `${service.duration}m ● ${service.price} ● ${service.notes}`
                        }
                    />
                </ListItem>
            )}
        </List >
    </Container>
}

export default ListServices;
