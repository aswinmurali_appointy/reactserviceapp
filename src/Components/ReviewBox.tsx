import { Avatar, Button, Card, CardActions, CardContent, CardHeader, Container, Grid, IconButton, makeStyles, Typography } from "@material-ui/core";
import { Rating } from "@mui/lab";

const useStyle = makeStyles({
    button: {
        backgroundColor: '#8333a4',
        color: 'white',
    },
    ratingarea: {
        paddingTop: '34px',
    },
    buttonFlexRight: {
        display: "flex",
        justifyContent: "flex-end"
    },
    thanksLabelPadding: {
        paddingTop: '30px',
    },
    root: {
        width: '400px',
        margin: '30px',
    }
});

const ReviewBox = () => {
    const style = useStyle();

    return <Card className={style.root}>
        <CardHeader
            avatar={<Avatar aria-label="recipe">Go</Avatar>}
            title="Bengaluru"
        />
        <CardContent>
            <Typography>
                Please rate your last experience
            </Typography>
            <Container className={style.ratingarea}>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography>Overall</Typography>
                    </Grid>
                    <Grid item>
                        <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
                    </Grid>
                </Grid>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography>Ease-of-use</Typography>
                    </Grid>
                    <Grid item>
                        <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
                    </Grid>
                </Grid>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography>Features</Typography>
                    </Grid>
                    <Grid item>
                        <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
                    </Grid>
                </Grid>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography>Look & Feel</Typography>
                    </Grid>
                    <Grid item>
                        <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
                    </Grid>
                </Grid>
                <Typography className={style.thanksLabelPadding} align="center">
                    " Thanks! "
                </Typography>
            </Container>
        </CardContent>
        <CardActions className={style.buttonFlexRight}>
            <Button className={style.button}>EDIT</Button>
        </CardActions>
    </Card>
}

export default ReviewBox;
